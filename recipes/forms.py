from django.forms import ModelForm, TextInput, CharField, URLField
from recipes.models import Recipe
from django import forms


class RecipeForm(ModelForm):
    class Meta:
        model = Recipe
        fields = [
            "title",
            "picture",
            "description",
            "new_recipe",
        ]

        # widgets = {
        #     "title": TextInput(
        #         attrs={
        #             "class": "form-control",
        #             "style": "width: 50%;",
        #             "style": "margin: 5px 5px 5px 0px;",
        #             "placeholder": "Name",
        #         }
        #     ),
        #     "picture": TextInput(
        #         attrs={
        #             "class": "form-control",
        #             "style": "width: 40%;",
        #             "style": "color: black",
        #             "style": "margin: 5px 5px 10px 0px;",
        #             "placeholder": "'https//sample-image'",
        #         }
        #     ),
        #     "description": forms.Textarea(
        #         attrs={
        #             "class": "form-control",
        #             "style": "width: 90%;",
        #             "style": "margin: 5px 5px 5px 0px;",
        #             "placeholder": "Description of your recipe.",
        #         }
        #     ),
        # }
