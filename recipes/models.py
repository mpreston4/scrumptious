from django.db import models
from django.conf import settings


class Recipe(models.Model):
    title = models.CharField(max_length = 200)
    description = models.TextField()
    picture = models.URLField()
    created_on = models.DateTimeField(auto_now_add=True)
    new_recipe = models.BooleanField(default=False)

    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,    # This is the recceomended way to refer to the user model.
        related_name = "recipes",
        on_delete=models.CASCADE,
        null=True,                   # When we add an attributre to an existing model, Django thinks there might be existing
        # data in the database, even if there isn't. When we add an attribute, it means that Django will add a column to
        # that table in the database. But, the rows may already exist. So what value woudl they have in these rows? When we write
        # null = True, we tell Django to tell the database that we just want the database to put an empty value in there.
    )


    def __str__(self):    # This allows us to see the "Recipe_objects" in our RecipeStep class in the admin page, rather than displaying "Recipe_object(1)", etc.
        return self.title



class RecipeStep(models.Model):
    step_number = models.PositiveSmallIntegerField()
    instruction = models.TextField()
    recipe = models.ForeignKey(
        Recipe,
        related_name = "steps",
        on_delete=models.CASCADE
    )

    class Meta:
        ordering = ['step_number']




    # def recipe_title(self):
    #     return self.recipe.title


class RecipeIngredients(models.Model):
    amount = models.CharField(max_length=100)
    food_item = models.CharField(max_length=100)

    recipe = models.ForeignKey(
        Recipe,
        related_name = "ingredients",
        on_delete=models.CASCADE
    )

    class Meta:
        ordering = ['food_item']
