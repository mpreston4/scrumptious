from django.urls import path
from accounts.views import signup, user_login, user_logout

urlpatterns = [
    path("signup/", signup, name = "signup"),
    path("login/", user_login, name = "login"), # Name = "login" is what we use in the html file. Ex: in the list file, we use {% url login %}
    path("", user_logout, name = "logout"),
]
